require 'test_helper'

class TimezonesControllerTest < ActionController::TestCase
  setup do
    @timezone = timezones(:timezone1)
  end

  # INDEX
  test "index should redirect to login page if not logged in" do
    get :index
    assert_response :found
    assert_redirected_to new_user_session_path
  end


  test "should get index if logged in" do
    login_user

    get :index
    assert_response :success
    assert assigns(:timezones)
    assert_equal 1, assigns(:timezones).length
  end

  # SEARCH
  test "should find timezone if logged in" do
    login_user

    # get :search, xhr: true, format: :json, :starts_with => "bla"
    xhr :get, :search, format: :js, :starts_with => 'Time'
    assert_response :success
    assert assigns(:timezones)
    assert_equal 1, assigns(:timezones).length
  end

  # NEW
  test "new should redirect to login page if not logged in" do
    get :new
    assert_response :found
    assert_redirected_to new_user_session_path
  end

  test "should get new if logged in" do
    login_user

    get :new
    assert_response :success
  end

  # CREATE
  test "should NOT create timezone if NOT logged in" do

    assert_no_difference('Timezone.count') do
      post :create, timezone: { city: @timezone.city, difference: @timezone.difference, name: @timezone.name, user_id: @timezone.user_id }
    end

    assert_response :found
    assert_redirected_to new_user_session_path
  end

  test "should create timezone if logged in" do
    login_user

    assert_difference('Timezone.count') do
      post :create, timezone: { city: @timezone.city, difference: @timezone.difference, name: @timezone.name, user_id: @timezone.user_id }
    end

    assert_response :success
  end

  # SHOW
  test "should NOT show timezone if NOT logged in" do
    get :show, id: @timezone
    assert_response :found
    assert_redirected_to new_user_session_path
  end

  test "should show timezone if logged in" do
    login_user

    get :show, id: @timezone
    assert_response :success
  end

  # EDIT
  test "should NOT get edit if NOT logged in" do
    get :edit, id: @timezone
    assert_response :found
    assert_redirected_to new_user_session_path
  end

  test "should get edit if logged in" do
    login_user

    get :edit, id: @timezone
    assert_response :success
  end

  # UPDATE
  test "should NOT update timezone if NOT logged in" do
    patch :update, id: @timezone, timezone: { city: @timezone.city, difference: @timezone.difference, name: @timezone.name, user_id: @timezone.user_id }
    assert_response :found
    assert_redirected_to new_user_session_path
  end


  test "should update timezone if logged in" do
    login_user

    patch :update, id: @timezone, timezone: { city: @timezone.city, difference: @timezone.difference, name: @timezone.name, user_id: @timezone.user_id }
    assert_response :success
  end

  # DESTROY
  test "should NOT destroy timezone if NOT logged in" do

    assert_no_difference('Timezone.count') do
      delete :destroy, id: @timezone
    end

    assert_response :found
    assert_redirected_to new_user_session_path
  end

  test "should destroy timezone if logged in" do
    login_user

    assert_difference('Timezone.count', -1) do
      delete :destroy, id: @timezone, :remote => true
    end

    assert_response :success
  end

  private

    def login_user
      user = users(:user1)
      sign_in user
    end
end

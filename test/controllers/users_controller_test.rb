require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @admin = users(:admin)
    @user = users(:user1)
    @user2 = users(:user2)
  end

  # CREATE
  test "should NOT create user if guest" do

    assert_no_difference('User.count') do
      post :create, user: user_hash(@user)
    end

    # make sure our authorization kicks in
    assert_response :found
    assert_redirected_to root_path
  end

  test "should NOT create admin if user" do
    sign_in @user

    assert_no_difference('User.count') do
      post :create, user: user_hash(@user2)
    end

    # make sure our authorization kicks in
    assert_response :found
    assert_redirected_to root_path
  end

  # UPDATE
  test "should NOT update to admin if user" do
    sign_in @user

    put :update, id: @user, user: user_hash(@user)

    # make sure we can't set an admin field
    assert_not assigns(:user).admin

    # everything else should be updatable
    assert_response :ok
  end

  test "should NOT update another user if user" do
    sign_in @user

    put :update, id: @user2, user: user_hash(@user2)

    # make sure our authorization kicks in
    assert_response :found
    assert_redirected_to root_path
  end

  test "should update to admin if admin" do
    sign_in @admin

    # let's update the user hash admin field
    uh = user_hash(@user)
    uh[:admin] = true

    put :update, id: @user, user: uh

    # make sure we set an admin field
    assert assigns(:user).admin
    assert_response :ok
  end

end

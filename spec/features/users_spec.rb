require 'rails_helper'

RSpec.feature "Users", type: :feature do

  scenario "should access his profile", :js => true, driver: :webkit do
    user = sign_in_user
    click_link 'Profile'
    expect(page).to have_content(user.email)
  end

  scenario "should not access another profile", :js => true, driver: :webkit do
    user = sign_in_user
    another_user = FactoryGirl.create(:user)

    visit show_user_path(another_user)
    expect(page).to have_content('not authorized')
  end

  describe "user updating his profile", :js => true, driver: :webkit do
    context "when not logged in" do
      it 'should not access any user profile' do
        user = FactoryGirl.create(:user)
        visit show_user_path(user)
        expect(page).to have_content('not authorized')
      end
    end

    context "when normal user" do
      before do
        @user = sign_in_user
      end

      it 'should update his user profile and password' do
        updated = FactoryGirl.build(:user)
        new_pass = "newsecretpassword"

        click_link 'Profile'
        click_link 'Edit'
        within '.simple_form' do
          fill_in 'Name', :with => updated.name
          fill_in 'Email', :with => updated.email
          fill_in 'Password', :with => new_pass
          fill_in 'Password confirmation', :with => new_pass
        end
        click_button 'Update'

        expect(page).to have_content('Updated')
        expect(page).to have_content(updated.name)
        expect(page).to have_content(updated.email)
      end

      it 'should update his user profile without updating password' do
        updated = FactoryGirl.build(:user)

        click_link 'Profile'
        click_link 'Edit'
        within '.simple_form' do
          fill_in 'Name', :with => updated.name
          fill_in 'Email', :with => updated.email
        end
        click_button 'Update'

        expect(page).to have_content('Updated')
        expect(page).to have_content(updated.name)
        expect(page).to have_content(updated.email)
      end

      it 'should not access another user profile' do
        another_user = FactoryGirl.create(:user)

        visit show_user_path(another_user)
        expect(page).to have_content('not authorized')
      end
    end

    context "when admin" do
      before do
        @admin = sign_in_admin
      end

      it 'should update any user profile' do
        another_user = FactoryGirl.create(:user)
        updated = FactoryGirl.build(:user)

        visit show_user_path(another_user)

        click_link 'Edit'
        within '.simple_form' do
          fill_in 'Name', :with => updated.name
          fill_in 'Email', :with => updated.email
        end
        click_button 'Update'

        expect(page).to have_content('Updated')
        expect(page).to have_content(updated.name)
        expect(page).to have_content(updated.email)
        expect(page).to have_content(updated.admin)
      end

      it 'should make any user an admin' do
        another_user = FactoryGirl.create(:user)
        updated = FactoryGirl.build(:admin)

        visit show_user_path(another_user)

        click_link 'Edit'
        within '.simple_form' do
          fill_in 'Name', :with => updated.name
          fill_in 'Email', :with => updated.email
          check 'Admin'
        end
        click_button 'Update'

        expect(page).to have_content('Updated')
        expect(page).to have_content(updated.name)
        expect(page).to have_content(updated.email)
        expect(page).to have_content(updated.admin)
      end
    end
  end

end

require 'rails_helper'

RSpec.describe "PasswordResets", type: :feature do

  it "emails user when requesting password reset" do
    user = FactoryGirl.create(:user)
    visit new_user_session_path
    click_link 'Forgot your password?'
    within '#reset_password_form' do
      fill_in 'Email', :with => user.email
    end
    click_button 'reset_password_submit'
    expect(current_path).to eq(new_user_session_path)
    expect(page).to have_content("You will receive an email")
    expect(ActionMailer::Base.deliveries.last.to).to eql([user.email])
  end

end

module AuthenticationHelper

  def sign_in_user(user = nil)
    user = FactoryGirl.create(:user) unless user
    visit new_user_session_path

    within '#user-login' do
      fill_in 'user_email', :with => user.email
      fill_in 'user_password', :with => user.password
    end

    click_button 'Log in'

    wait_for_ajax

    expect(current_path).to eq(timezones_path)
    expect(page).to have_content("Signed in successfully")

    return user
  end

  def sign_in_admin
    user = FactoryGirl.create(:admin)
    sign_in_user user
  end

  def get_auth_token(user = nil)
    user = FactoryGirl.create(:user) unless user
    post new_api_user_session_path, email: user.email, password: user.password
    response.headers['access-token']
    user
  end
end

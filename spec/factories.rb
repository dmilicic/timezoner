FactoryGirl.define do

  factory :timezone do
    sequence(:name) { |n| "name#{n}" }
    sequence(:city) { |n| "city#{n}" }
    sequence(:difference) { |n| n % 12 }
    user
  end

  factory :user do
    sequence(:email) { |n| "name#{n}@gmail.com" }
    password "secretpassword"
    sequence(:name) { |n| "foo#{n}" }
    confirmed_at DateTime.now
    admin false

    factory :admin do
      sequence(:name) { |n| "admin#{n}" }
      sequence(:email) { |n| "admin#{n}@timezoner.com" }
      admin true
    end

    # user_with_posts will create post data after the user has been created
    factory :user_with_timezones do
      # posts_count is declared as a transient attribute and available in
      # attributes on the factory, as well as the callback via the evaluator
      transient do
        timezones_count 10
      end

      # the after(:create) yields two values; the user instance itself and the
      # evaluator, which stores all values from the factory, including transient
      # attributes; `create_list`'s second argument is the number of records
      # to create and we make sure the user is associated properly to the post
      after(:create) do |user, evaluator|
        create_list(:timezone, evaluator.timezones_count, user: user)
      end
    end
  end

end

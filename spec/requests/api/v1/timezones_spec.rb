require 'rails_helper'

RSpec.describe "Api::V1::Timezones", type: :request do

  describe "POST /api/v1/timezones" do
      context 'when not logged in' do
          it "shouldn't create new timezone" do
            post api_timezones_path, name: 'name', city: 'city', difference: 5
            expect(response).to have_http_status(401)
          end
      end

      context 'when logged in' do
          before do
            get_auth_token
          end

          it "should create a new timezone" do
            timezone = FactoryGirl.build(:timezone)

            params = { name: timezone.name, city: timezone.city, difference: timezone.difference }.to_json
            post api_timezones_path, params, headers

            expect(response).to have_http_status(201)
            expect(json['name']).to eq(timezone.name)
          end

          it "shouldn't create invalid timezone" do
            params = { name: nil, city: 'city', difference: 5 }
            post api_timezones_path, params.to_json, headers

            expect(response).to have_http_status(422)
            expect(json['name']).to include("can't be blank")
          end
      end

      context 'when admin' do
          before do
            @admin = FactoryGirl.create(:admin)
            get_auth_token @admin
          end

          it 'should create timezone for any user' do
            timezone = FactoryGirl.build(:timezone)
            user = timezone.user

            params = { :timezone => {name: timezone.name, city: timezone.city, difference: timezone.difference, email: user.email} }.to_json
            post api_timezones_path, params, headers

            expect(response).to have_http_status(201)
            expect(json['name']).to eq(timezone.name)
            expect(json['user_id']).to eq(user.id)
          end

          it 'should create timezone for himself if no email given' do
            timezone = FactoryGirl.build(:timezone)
            user = FactoryGirl.create(:user)

            params = { :timezone => {name: timezone.name, city: timezone.city, difference: timezone.difference} }.to_json
            post api_timezones_path, params, headers

            expect(response).to have_http_status(201)
            expect(json['name']).to eq(timezone.name)
            expect(json['user_id']).to eq(@admin.id)
          end
      end
  end

  describe "GET /api/v1/timezones" do
    context 'when not logged in' do
        it "shouldn't access timezones" do
          get api_timezones_path
          expect(response).to have_http_status(401)
        end
    end

    context 'when logged in' do
        it "should get all user's timezones" do
          user = FactoryGirl.create(:user_with_timezones)
          get_auth_token user

          get api_timezones_path, headers
          expect(response).to have_http_status(200)

          # we should only get the timezones of the first user
          expect(json.length).to eq(10)
        end

        it "has no timezones" do
          get_auth_token

          get api_timezones_path, headers
          expect(response).to have_http_status(200)

          # we should only get the timezones of the first user
          expect(json.length).to eq(0)
        end
    end

    context 'when admin' do
        before do
          @admin = FactoryGirl.create(:admin)
          get_auth_token @admin
        end

        it "should get all timezones of all users" do
          # create users with a bunch of timezones
          user = FactoryGirl.create(:user_with_timezones)
          another_user = FactoryGirl.create(:user_with_timezones)

          get api_timezones_path, headers
          expect(response).to have_http_status(200)

          total_timezone_count = user.timezones.count + another_user.timezones.count
          expect(json.length).to eq(total_timezone_count)
        end
    end
  end


  describe "GET /api/v1/search/timezones" do
    context 'when not logged in' do
        it "shouldn't search timezones" do
          get api_search_timezones_path
          expect(response).to have_http_status(401)
        end
    end

    context 'when logged in' do
        it "should filter user's timezones" do
          user = FactoryGirl.create(:user_with_timezones)
          get_auth_token user

          # all timezones start with 'na'
          headers['starts_with'] = user.timezones.first.name[0..2]

          get api_search_timezones_path, headers
          expect(response).to have_http_status(200)

          # we should only get the timezones of the first user
          expect(json.length).to eq(10)
        end

        it "should filter user's timezones" do
          user = FactoryGirl.create(:user_with_timezones)
          get_auth_token user

          # all timezones start with 'na' so we should filter all
          headers['starts_with'] = 'd12ryzsc'

          get api_search_timezones_path, headers
          expect(response).to have_http_status(200)

          # everything should be filtered
          expect(json.length).to eq(0)
        end

        it "has no timezones" do
          get_auth_token

          get api_timezones_path, headers
          expect(response).to have_http_status(200)

          # we should only get the timezones of the first user
          expect(json.length).to eq(0)
        end
    end
  end


  describe "GET /api/v1/timezone/:id" do
    context 'when not logged in' do
        it "shouldn't get timezone data" do
          timezone = FactoryGirl.create(:timezone)
          get api_timezone_path(timezone)
          expect(response).to have_http_status(401)
        end
    end

    context 'when logged in' do
        it "should get timezone data" do
          timezone = FactoryGirl.create(:timezone)

          get_auth_token timezone.user

          get api_timezone_path(timezone), headers
          expect(response).to have_http_status(200)
          expect(json['name']).to eq(timezone.name)
        end

        it "shouldn't get timezone of different user" do
          timezone = FactoryGirl.create(:timezone)
          another_timezone = FactoryGirl.create(:timezone)

          get_auth_token timezone.user

          get api_timezone_path(another_timezone), headers
          expect(response).to have_http_status(403)
        end
    end

    context 'when admin' do
        before do
          @admin = FactoryGirl.create(:admin)
          get_auth_token @admin
        end

        it "should get timezone data for any user" do
          timezone = FactoryGirl.create(:timezone)

          get api_timezone_path(timezone), headers
          expect(response).to have_http_status(200)

          expect(json['name']).to eq(timezone.name)
          expect(json['city']).to eq(timezone.city)
          expect(json['difference']).to eq(timezone.difference)
          expect(json['user_id']).to eq(timezone.user.id)
        end
    end
  end

  describe "PUT /api/v1/timezone/:id" do
    context 'when not logged in' do
        it "shouldn't update timezone data" do
          timezone = FactoryGirl.create(:timezone)
          updated_timezone = FactoryGirl.build(:timezone)
          params = { name: updated_timezone.name, city: updated_timezone.city }.to_json
          put api_timezone_path(timezone), params, headers
          expect(response).to have_http_status(401)
        end
    end

    context 'when logged in' do
        it "should update timezone data" do
          timezone = FactoryGirl.create(:timezone)
          updated_timezone = FactoryGirl.build(:timezone)

          # login our user
          get_auth_token timezone.user

          params = { name: updated_timezone.name, city: updated_timezone.city }.to_json
          put api_timezone_path(timezone), params, headers

          expect(response).to have_http_status(200)
          expect(json['name']).to eq(updated_timezone.name)
          expect(json['city']).to eq(updated_timezone.city)
        end

        it "shouldn't update timezone of different user" do
          timezone = FactoryGirl.create(:timezone)
          updated_timezone = FactoryGirl.create(:timezone)

          # we can only update this timezone if it belongs to us
          updated_timezone.user = timezone.user

          # login our user
          get_auth_token timezone.user

          # lets update the timezone of a different user
          params = { name: timezone.name, city: timezone.city, difference: timezone.difference }.to_json
          put api_timezone_path(updated_timezone), params, headers

          expect(response).to have_http_status(403)
        end
    end

    context 'when admin' do
        before do
          @admin = FactoryGirl.create(:admin)
          get_auth_token @admin
        end

        it "should update timezone of any user" do
          timezone = FactoryGirl.create(:timezone)
          updated_timezone = FactoryGirl.build(:timezone)

          params = { name: updated_timezone.name, city: updated_timezone.city }.to_json
          put api_timezone_path(timezone), params, headers

          expect(response).to have_http_status(200)
          expect(json['name']).to eq(updated_timezone.name)
          expect(json['city']).to eq(updated_timezone.city)
        end
    end
  end

  describe "DELETE /api/v1/timezone/:id" do
    context 'when not logged in' do
        it "shouldn't delete timezone" do
          timezone = FactoryGirl.create(:timezone)
          delete api_timezone_path(timezone), headers
          expect(response).to have_http_status(401)
        end
    end

    context 'when logged in' do
        it "should destroy timezone" do
          timezone = FactoryGirl.create(:timezone)

          get_auth_token timezone.user

          delete api_timezone_path(timezone), headers

          expect(response).to have_http_status(200)
          expect(json['name']).to eq(timezone.name)
        end

        it "should not destroy unexisting timezone" do
          get_auth_token

          delete api_timezone_path(id: 51203), headers

          expect(response).to have_http_status(404)
        end

        it "shouldn't destroy timezone of different user" do
          timezone = FactoryGirl.create(:timezone)
          another_timezone = FactoryGirl.create(:timezone)

          get_auth_token timezone.user

          delete api_timezone_path(another_timezone), headers

          expect(response).to have_http_status(403)
        end
    end

    context 'when admin' do
        before do
          @admin = FactoryGirl.create(:admin)
          get_auth_token @admin
        end

        it "should destroy timezone of any user" do
          timezone = FactoryGirl.create(:timezone)

          delete api_timezone_path(timezone), headers

          expect(response).to have_http_status(200)
          expect(json['name']).to eq(timezone.name)
          expect(json['user_id']).to eq(timezone.user.id)
        end
    end
  end

end

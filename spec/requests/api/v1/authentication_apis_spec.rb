require 'rails_helper'

RSpec.describe "AuthenticationApis", type: :request, focus: true do


  describe "GET /auth" do

    it "should get auth token" do
      user = FactoryGirl.create(:user)
      post new_api_user_session_path, email: user.email, password: user.password
      expect(response).to have_http_status(200)
      expect(response.headers).to include('access-token')
    end

    it "should not get auth token" do
      user = FactoryGirl.create(:user)
      post new_api_user_session_path, email: user.email, password: "wrongpassword"
      expect(response).to have_http_status(401)
      expect(response.headers).not_to include('access-token')
    end
  end
end

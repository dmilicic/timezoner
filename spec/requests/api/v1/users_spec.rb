require 'rails_helper'

RSpec.describe "Api::V1::Users", type: :request do


    describe "POST /api/v1/users" do

        context 'when not logged in' do
            it "shouldn't create new user" do
              post api_users_path, { :user => {name: 'name', email: 'user@gmail.com', confirmed_at: DateTime.now, password: "password"} }
              expect(response).to have_http_status(401)
            end
        end

        context 'when normal user' do
            before do
              get_auth_token
            end

            it "shouldn't create new user" do
              user = FactoryGirl.build(:user)

              params = { :user => {name: user.name, email: user.email, password: user.password} }.to_json
              post api_users_path, params, headers

              expect(response).to have_http_status(403)
            end
        end

        context 'when admin' do
            before do
              @admin = FactoryGirl.create(:admin)
              get_auth_token @admin
            end

            it "should create new user" do
              user = FactoryGirl.build(:user)

              params = { :user => {name: user.name, email: user.email, password: user.password} }.to_json
              post api_users_path, params, headers

              expect(response).to have_http_status(201)
              expect(json['name']).to eq(user.name)
            end

            it "should create new admin" do
              admin = FactoryGirl.build(:admin)

              params = { :user => {name: admin.name, email: admin.email, password: admin.password, admin: admin.admin} }.to_json
              post api_users_path, params, headers

              expect(response).to have_http_status(201)
              expect(json['name']).to eq(admin.name)
              expect(json['admin']).to eq(true)
            end

            it "shouldn't create invalid user" do
              user = User.new

              params = { :user => {email: user.email, password: user.password} }.to_json
              post api_users_path, params, headers

              expect(response).to have_http_status(422)
              expect(json['name']).to include("can't be blank")
            end

            it "shouldn't create user with empty params" do
              post api_users_path, headers
              expect(response).to have_http_status(400)
            end
        end
    end

    describe "GET /api/v1/users" do

        context 'when not logged in' do
            it "shouldn't get users" do
              get api_users_path
              expect(response).to have_http_status(401)
            end
        end

        context 'when normal user' do
            before do
              get_auth_token
            end

            it "shouldn't get users" do
              get api_users_path, headers
              expect(response).to have_http_status(403)
            end
        end

        context 'when admin' do
            before do
              @admin = FactoryGirl.create(:admin)
              get_auth_token @admin
            end

            it "should get all users" do
              user = FactoryGirl.create_list(:user, 10)

              get api_users_path, headers

              expect(response).to have_http_status(200)
              expect(json.length).to eq(11) # 10 plus himself
            end
        end
    end

    describe "GET /api/v1/user/:id" do
      context "when not logged in" do
        it "shouldn't get user data" do
          user = FactoryGirl.create(:user)
          get api_user_path(user)
          expect(response).to have_http_status(401)
        end
      end

      context "when normal user" do
        it 'should get his user data' do
          user = get_auth_token

          get api_user_path(user), headers
          expect(response).to have_http_status(200)

          expect(json.keys).to contain_exactly("name", "email", "created_at", "updated_at")
          expect(json['name']).to eq(user.name)
          expect(json['email']).to eq(user.email)
        end

        it "shouldn't get different user data" do
          user = FactoryGirl.create(:user)
          another_user = FactoryGirl.create(:user)
          get_auth_token user

          get api_user_path(another_user), headers
          expect(response).to have_http_status(403)
        end
      end

      context "when admin" do
        before do
          @admin = FactoryGirl.create(:admin)
          get_auth_token @admin
        end

        it 'should get any user data' do
          user = FactoryGirl.create(:user)

          get api_user_path(user), headers
          expect(response).to have_http_status(200)

          expect(json['name']).to eq(user.name)
          expect(json['email']).to eq(user.email)
          expect(json['admin']).to eq(user.admin)
          expect(json['id']).to eq(user.id)
        end
      end
    end

    describe "PUT /api/v1/user/:id" do
      context "when not logged in" do
        it "shouldn't update any user data" do
          user = FactoryGirl.create(:user)
          params = { name: user.name }.to_json
          put api_user_path(user), params
          expect(response).to have_http_status(401)
        end
      end

      context "when normal user" do
        before do
          @user = get_auth_token
        end

        it "should update his data with updating password" do
          updated_user = FactoryGirl.build(:user)

          new_pass = "newpassword"
          params = { :user => {name: updated_user.name, email: updated_user.email, password: new_pass, password_confirmation: new_pass } }.to_json
          put api_user_path(@user), params, headers

          expect(response).to have_http_status(200)
          expect(json['name']).to eq(updated_user.name)
          expect(json['email']).to eq(updated_user.email)

          expect(assigns[:user].password).to eq(new_pass)
        end

        it "shouldn't update other user's data" do
          other_user = FactoryGirl.create(:user)
          params = { name: @user.name, email: @user.email, password: "newpassword", password_confirmation: "newpassword" }.to_json
          put api_user_path(other_user), params, headers
          expect(response).to have_http_status(403)
        end
      end

      context "when admin" do
        before do
          @admin = FactoryGirl.create(:admin)
          get_auth_token @admin
        end

        it 'should update any user data' do
          user = FactoryGirl.create(:user)
          other_user = FactoryGirl.build(:user)

          new_pass = "newpassword"
          params = { :user => {name: other_user.name, email: other_user.email, password: new_pass, password_confirmation: new_pass } }.to_json
          put api_user_path(user), params, headers

          expect(response).to have_http_status(200)
          expect(json['name']).to eq(other_user.name)
          expect(json['email']).to eq(other_user.email)

          # admin should be able to update the password here
          expect(assigns[:user].password).to eq(new_pass)
        end
      end
    end


    describe "DELETE /api/v1/user/:id" do
      context "when not logged in" do
        it "shouldn't delete any user" do
          user = FactoryGirl.create(:user)
          delete api_user_path(user), headers
          expect(response).to have_http_status(401)
        end
      end

      context "when normal user" do
        before do
          @user = get_auth_token
        end

        it "shouldn't delete any user" do
          delete api_user_path(@user), headers
          expect(response).to have_http_status(403)
        end
      end

      context "when admin" do
        before do
          @admin = FactoryGirl.create(:admin)
          get_auth_token @admin
        end

        it 'should delete any user' do
          user = FactoryGirl.create(:user)
          delete api_user_path(user), headers
          expect(response).to have_http_status(200)

          expect(json['name']).to eq(user.name)
          expect(json['email']).to eq(user.email)
        end
      end
    end


    describe "SEARCH /api/v1/search/users" do
      context "when not logged in" do
        it "shouldn't search users" do
          get api_search_users_path
          expect(response).to have_http_status(401)
        end
      end

      context "when normal user" do
        it "shouldn't search users" do
          user = get_auth_token
          get api_search_users_path, headers
          expect(response).to have_http_status(403)
        end
      end

      context "when admin" do
        before do
          @admin = FactoryGirl.create(:admin)
          get_auth_token @admin
        end

        it 'should search users' do
          user_cnt = 10
          users = FactoryGirl.create_list(:user, user_cnt)

          # all start with 'foo'
          headers['starts_with'] = users.first.name[0..2]

          get api_search_users_path, headers
          expect(response).to have_http_status(200)

          expect(json.length).to eq(user_cnt)
        end
      end
    end

end

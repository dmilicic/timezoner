class DeviseTokenAuthCreateUsers < ActiveRecord::Migration
  def change
    ## Required
    add_column :users, :provider, :string , :null => false, :default => "email"
    add_column :users, :uid, :string , :null => false, :default => ""

    ## Confirmable
    add_column :users, :confirmation_token, :string
    add_column :users, :confirmed_at, :datetime
    add_column :users, :confirmation_sent_at, :datetime
    add_column :users, :unconfirmed_email, :string  # Only if using reconfirmable

    ## Tokens
    add_column :users, :tokens, :string

    add_index :users, :confirmation_token,   :unique => true
    # add_index :users, :unlock_token,         :unique => true
  end
end

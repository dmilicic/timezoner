class CreateTimezones < ActiveRecord::Migration
  def change
    create_table :timezones do |t|
      t.string :name
      t.string :city
      t.integer :difference
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

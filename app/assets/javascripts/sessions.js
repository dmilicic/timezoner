$(document).on('page:change', function(e) {

  // display a message that the delete action was successful
  $("#user-login")
    .bind("ajax:error", function(event, data, status, xhr) {

      // print the error message
      $("#messages").replaceWith(data.responseText);
  });
});

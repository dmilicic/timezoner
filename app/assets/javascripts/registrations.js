$(document).on('page:change', function(e) {

  // display a message that the delete action was successful
  $("#signup_form")
    .bind("ajax:error", function(event, data, status, xhr) {

      // print the error message
      $("#new_user").replaceWith(data.responseText);
  });
});

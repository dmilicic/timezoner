$(document).on('page:change', function() {

  // display a message that the delete action was successful
  $("#user-table-div").bind("ajax:success", function(event, data, status, xhr) {

      $("#user-notice").removeClass("hidden");

      user_row = "#user_" + data.id;
      $(user_row).remove();
  });

  $("#user-search-form").bind("ajax:success", function(event, data, status, xhr) {
    $('#user-table').replaceWith(data);
  });


  $("#user_create").bind("ajax:error", function(event, data, status, xhr) {

    // print the error messages
    $("form").replaceWith(data.responseText);
  });

  $("#user_edit").bind("ajax:error", function(event, data, status, xhr) {

    // print the error messages
    $("form").replaceWith(data.responseText);
  });
});

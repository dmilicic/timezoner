$(document).ready(function() {

  // make a clock tick in real time
  var clock = function() {
    $(".time").each(function () {

      var date_str = $(this).text();
      var date = new Date(date_str);

      date.setSeconds(date.getSeconds() + 1);
      date_str = date.toString('ddd, dd MMM yyyy HH:mm:ss');

      $(this).html("<strong>" + date_str + "</strong>");
    });

    setTimeout(clock, 1000);
  }

  // tick every second
  setTimeout(clock, 1000);
});


$(document).on('page:change', function() {

  // display a message that the delete action was successful
  $("#timezone-table-div").bind("ajax:success", function(event, data, status, xhr) {

      $("#timezone-notice").removeClass("hidden");

      timezone_row = "#timezone_" + data.id;
      $(timezone_row).remove();
  });


  $("#timezone-search-form").bind("ajax:success", function(event, data, status, xhr) {
    $('#timezone-table').replaceWith(data);
  });



  // display a message that the create action failed
  $("#timezone_create").bind("ajax:error", function(event, data, status, xhr) {

      // print the error messages
      $("#new_timezone").replaceWith(data.responseText);
  });



  // display a message that the edit action failed
  $("#timezone_edit").bind("ajax:error", function(event, data, status, xhr) {

      // print the error messages
      $("form").replaceWith(data.responseText);
  });
});

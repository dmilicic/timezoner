if current_user.admin?
  json.extract! @user, :id, :name, :email, :admin, :created_at, :updated_at
else
  json.extract! @user, :name, :email, :created_at, :updated_at
end

json.array!(@timezones) do |timezone|
  json.extract! timezone, :id, :name, :city, :difference, :user_id
  json.url timezone_url(timezone, format: :json)
end

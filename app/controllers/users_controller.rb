class UsersController < ApplicationController
  load_and_authorize_resource # authorization

  before_action :authenticate_user!

  def index
    puts @users
  end

  def search
    if request.xhr?
      @users = User.filter filtered_params
      render :text => render_to_string(:partial => 'user_table', layout: false), status: :ok
    else
      head :not_found
    end
  end

  def show
  end

  def new
    @minimum_password_length = (Devise.password_length).first # get the minimum length
  end

  def edit
  end

  def create
    if @user.save
      redirect_via_turbolinks_to users_path, status: :created, notice: 'User was successfully created.'
    else
      render json: render_to_string(:partial => 'form', layout: false), status: :unprocessable_entity
    end
  end

  def update

    # password can be blank while updating
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    if @user.update(user_params)
      sign_in @user, :bypass => true unless current_user.admin?
      redirect_via_turbolinks_to show_user_path(@user), status: :ok, notice: 'Updated!'
    else
      render json: render_to_string(:partial => 'form_edit', layout: false), status: :unprocessable_entity
    end
  end

  def destroy
    @user.destroy
    render json: @user
  end

  private

    def filtered_params
      params.slice(:starts_with)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      if !current_user.nil? && current_user.admin?
        params.require(:user).permit(:name, :email, :admin, :password, :password_confirmation)
      else
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
      end
    end

end

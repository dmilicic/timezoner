class TimezonesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource # authorization

  # GET /timezones
  def index
    @timezones, @local_times = current_user.get_timezones filtered_params
  end

  def search
    if request.xhr?
      @timezones, @local_times = current_user.get_timezones filtered_params
      render :text => render_to_string(:partial => 'timezone_table', layout: false), status: :ok
    else
      head :not_found
    end
  end

  # GET /timezones/1
  def show
    @local_time = Time.now.utc.advance(hours: @timezone.difference).strftime(Timezone.DATE_FORMAT)
  end

  # GET /timezones/new
  def new
  end

  # GET /timezones/1/edit
  def edit
  end

  # POST /timezones
  # POST /timezones.json
  def create
    @timezone.user_id = current_user.id

    # attach the timezone to a different user if an admin requests so
    admin_attach_user_id

    if @timezone.save
      redirect_via_turbolinks_to timezones_path, status: :created, notice: 'Timezone was successfully created.'
    else
      render json: render_to_string(:partial => 'form', layout: false), status: :unprocessable_entity
    end
  end

  # PATCH/PUT /timezones/1
  # PATCH/PUT /timezones/1.json
  def update
    @timezone.user_id = current_user.id

    # attach the timezone to a different user if an admin requests so
    admin_attach_user_id

    respond_to do |format|
      if @timezone.update(timezone_params)
        format.html { redirect_via_turbolinks_to timezones_path, notice: 'Timezone was successfully updated.' }
        format.json { render :show, status: :ok, location: @timezone }
      else
        format.html { render json: render_to_string(:partial => 'form', layout: false), status: :unprocessable_entity }
        format.json { render json: @timezone.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /timezones/1
  # DELETE /timezones/1.json
  def destroy
    @timezone.destroy
    render json: @timezone
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def timezone_params
      params.require(:timezone).permit(:name, :city, :difference, :user_id, :email)
    end

    def filtered_params
      params.slice(:starts_with)
    end

    # attach the timezone to a different user if an admin requests so
    def admin_attach_user_id

      if current_user.admin?

        user_email = params[:timezone][:email] if params[:timezone][:email]
        if user_email
          user = User.find_by(email: user_email)
          @timezone.user_id = user.id if user
        else
          @timezone.user_id = current_user.id
        end

      end
    end


end

class User::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    resource = User.find_for_database_authentication(email: params[:user][:email])
    return invalid_login_attempt unless resource

    if resource.valid_password?(params[:user][:password])
      sign_in :user, resource
      set_flash_message(:notice, :signed_in)
      return redirect_via_turbolinks_to timezones_path
    end

    invalid_login_attempt
  end

  # DELETE /resource/sign_out
  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message :notice, :signed_out if signed_out
    yield if block_given?
    return redirect_via_turbolinks_to root_path
  end

  protected

  def invalid_login_attempt
    set_flash_message(:alert, :invalid, :now => true)
    render json: render_to_string(:layout => false, :partial => 'layouts/messages'), status: :unauthorized
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end

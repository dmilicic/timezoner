class Api::V1::TimezonesController < Api::V1::ApiController
  before_action :authenticate_user!
  load_and_authorize_resource  # authorization
  respond_to :json

  def index
    @timezones, @local_times = current_user.get_timezones filtered_params
  end

  def show
  end

  def search
    @timezones, @local_times = current_user.get_timezones filtered_params
    render 'index', status: :ok
  end

  def create
    @timezone.user_id = current_user.id

    # attach the timezone to a different user if an admin requests so
    admin_attach_user_id

    if @timezone.save
      render status: :created
    else
      render json: @timezone.errors, status: :unprocessable_entity
    end
  end

  def update
    @timezone.user_id = current_user.id

    # attach the timezone to a different user if an admin requests so
    admin_attach_user_id

    if @timezone.update(timezone_params)
      render status: :ok
    else
      render json: @timezone.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @timezone
      @timezone.destroy
      render status: :ok
    else
      head :not_found
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def timezone_params
      params.require(:timezone).permit(:name, :city, :difference, :user_id, :email)
    end

    def filtered_params
      params.slice(:starts_with)
    end

    # attach the timezone to a different user if an admin requests so
    def admin_attach_user_id

      if current_user.admin?

        user_email = params[:timezone][:email] if params[:timezone][:email]
        if user_email
          user = User.find_by(email: user_email)
          @timezone.user_id = user.id if user
        else
          @timezone.user_id = current_user.id
        end

      end
    end
end

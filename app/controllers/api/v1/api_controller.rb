class Api::V1::ApiController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken

  protect_from_forgery with: :null_session

  rescue_from ActionController::ParameterMissing do
    head status: :bad_request
  end

  rescue_from ActiveRecord::RecordNotFound do
    head status: :not_found
  end

  rescue_from CanCan::AccessDenied do |exception|
    render json: { :error => exception.message }, status: :forbidden
    # render json: exception, status: :forbidden
  end

end

class Api::V1::UsersController < Api::V1::ApiController
  before_action :authenticate_user!
  load_and_authorize_resource # authorization
  respond_to :json

  def index
  end

  def search
    @users = User.filter filtered_params
    render 'index', status: :ok
  end

  def show
  end

  def create
    # puts params
    if @user.save
      render status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update

    # password can be blank while updating
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    if @user.update(user_params)
      render status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @user
      @user.destroy
      render status: :ok
    else
      head :not_found
    end
  end

  private

    def filtered_params
      params.slice(:starts_with)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      if !current_user.nil? && current_user.admin?
        params.require(:user).permit(:name, :email, :admin, :password, :password_confirmation)
      else
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
      end
    end

end

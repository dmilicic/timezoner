class Timezone < ActiveRecord::Base
  scope :starts_with, -> (name) { where("name like ?", "#{name}%")}

  include Filterable

  DATE_FORMAT = '%a, %d %b %Y %H:%M:%S'

  belongs_to :user
  validates_presence_of :name, :city, :user_id
  validates :difference, :inclusion => { :in => -12..12, :message => "must be between -12 and 12, inclusive" }, :presence => true

  # email of the user who created this timezone object, required for simple_form input
  attr_accessor :email

  def self.DATE_FORMAT
    DATE_FORMAT
  end

end

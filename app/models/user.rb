class User < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User
  include Filterable

  scope :starts_with, -> (name) { where("name like ?", "#{name}%")}

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :name

  has_many :timezones, dependent: :destroy

  def get_timezones(filtered_params)

    if self.admin?
      @timezones = Timezone.filter filtered_params # admin can manage all timezones
    else
      @timezones = self.timezones.filter filtered_params # a user can only manage his own timezones
    end

    # convert to local times for each timezone
    @local_times = Hash.new
    @timezones.each do |zone|
      @local_times[zone.id] = Time.now.utc.advance(hours: zone.difference).strftime(Timezone.DATE_FORMAT)
    end

    return @timezones, @local_times
  end

end

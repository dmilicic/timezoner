class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # in case this is a guest user

    if user.admin?
      can :manage, :all
    else
      can :update, User do |updated_user|
        updated_user && updated_user == user
      end

      can :show, User do |shown_user|
        shown_user && shown_user == user
      end

      # Timezones
      can :manage, Timezone do |timezone|
        timezone.try(:user) == user
      end
      can :create, Timezone
    end
  end
end

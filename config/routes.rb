Rails.application.routes.draw do
  resources :timezones
  get '/search/timezones' => 'timezones#search'

  root to: 'visitors#index'


  devise_for :users, path_names: {sign_in: "login", sign_out: "logout"}, controllers: {
    sessions: 'user/sessions',
    registrations: 'user/registrations'
  }

  scope '/admin' do
    resources :users, except: [:show, :edit]
  end
  get '/users/:id' => 'users#show', as: "show_user"
  get '/users/:id/edit' => 'users#edit', as: "edit_user"
  get '/search/users' => 'users#search'

  ##### API ROUTES #####

  # token auth routes available at /api/v1/auth
  namespace :api, defaults: { format: :json } do
    scope :v1 do
      mount_devise_token_auth_for 'User', at: 'auth'
    end

    namespace :v1, as: nil do
      resources :timezones, except: [:new, :edit]
      get '/search/timezones' => 'timezones#search'

      resources :users, except: [:new, :edit]
      get '/search/users' => 'users#search'
    end
  end
end
